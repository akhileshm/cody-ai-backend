const express = require("express");
const router = express.Router();
require("dotenv").config();
const user = require("./user");
const ai = require("./ai");

// middleware
//const { verifyToken } = require("../../middleware/authJwt");

const profile = require("./profile");

router.use(user);
//router.use(verifyToken);
router.use(profile);
router.use(ai);

module.exports = router;
