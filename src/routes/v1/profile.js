const express = require("express");
const router = express.Router();
const profileController = require("../../controllers/profileController");
const { resetPasswordRules, validate } = require("../../middleware/validation");

// Translate SQL
router.get("/profile", profileController.getProfile);
router.post(
  "/resetPassword",
  resetPasswordRules(),
  validate,
  profileController.resetPassword
);

module.exports = router;
