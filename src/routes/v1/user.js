const express = require("express");
const router = express.Router();
const userController = require("../../controllers/userController");

router.post("/login", userController.Login);
router.post("/create-user", userController.createUser);
router.post("/verify-otp", userController.verifyOtp);
router.post("/update-industry", userController.updateIndustry);
router.post("/send-message", userController.sendMessage);
router.post("/summarize-conversation", userController.summarizeConversation);
router.post("/summarize-srs", userController.summarizeSRS);

module.exports = router;
