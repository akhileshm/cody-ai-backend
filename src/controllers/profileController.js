const bcrypt = require("bcrypt");

const getProfile = async (req, res, next) => {
  console.log("eneterde");
  try {
    const userId = req.userId;

    if (!userId) next(AppError.badRequest());
    console.log("eneterde2");
    const user = await models.User.findByPk(userId, {
      attributes: { exclude: ["password"] },
      include: [
        {
          model: models.Role,
          attributes: ["id", "name"],
        },
      ],
    });
    return res.status(200).json(user);
  } catch (error) {
    console.log(error);
    next(AppError.internal());
  }
};

const resetPassword = async (req, res, next) => {
  try {
    const { oldPassword, newPassword, confirmPassword } = req.body;
    const user = await models.User.findByPk(req.userId);

    if (!user) {
      return res.status(404).send("User not found.");
    }

    const validPassword = await bcrypt.compare(oldPassword, user.password);
    if (!validPassword) {
      return res.status(400).send("Old password is incorrect.");
    }

    if (newPassword !== confirmPassword) {
      return res
        .status(400)
        .send("New password and confirm password do not match.");
    }

    // Hash new password and save it
    user.password = bcrypt.hashSync(newPassword, 10);
    await user.save();

    res.status(200).send("Password has been reset successfully.");
  } catch (error) {
    console.log(error);
    next(AppError.internal());
  }
};

module.exports = {
  getProfile,
  resetPassword,
};
