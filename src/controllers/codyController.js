const axios = require("axios");

const createConversation = async (name, botId) => {
  const response = await axios.post(
    `${process.env.CODY_PORTAL}/conversations`,
    {
      name,
      bot_id: botId,
    },
    {
      headers: {
        Authorization: `Bearer ${process.env.CODY_ACCESS_TOKEN}`,
        "Content-Type": "application/json",
      },
    }
  );

  return response.data.data.id;
};

module.exports = { createConversation };
