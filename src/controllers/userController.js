const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
const axios = require("axios");
const { createConversation } = require("../controllers/codyController");
const { GoogleGenerativeAI } = require("@google/generative-ai");
const { Document, Packer, Paragraph, HeadingLevel, TextRun } = require("docx");
const { v4: uuidv4 } = require("uuid");
const fs = require("fs");
const twilio = require("twilio");
const twilioClient = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

const MODEL_NAME = "gemini-1.5-flash";
const API_KEY = process.env.GEMINI_API_KEY;

const Login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    // Validation
    if (!email) {
      return next(AppError.badRequest("Please Enter Email ID"));
    }

    if (!password) {
      return next(AppError.badRequest("Please Enter Password"));
    }

    // Query to check login credentials from database
    models.User.findOne({ where: { email } })
      .then((user) => {
        if (!user) {
          return next(AppError.unAuthorized());
        }

        const passwordIsValid = bcrypt.compareSync(password, user.password);
        if (!passwordIsValid) {
          return next(AppError.unAuthorized());
        }

        const token = jwt.sign({ id: user.id }, process.env.JWT_KEY, {
          expiresIn: 86400, // 24 hours
        });

        res.status(200).send({ access_token: token });
      })
      .catch((error) => {
        console.error(error);
        next(AppError.internal());
      });
  } catch (error) {
    console.error(error);
    next(AppError.internal());
  }
};

const createUser = async (req, res, next) => {
  const { name, email, phone } = req.body;

  try {
    // Generate 4-digit OTP
    const botId = process.env.CODY_BOT_ID;
    const otp = Math.floor(1000 + Math.random() * 9000).toString(); // Generates a 4-digit OTP
    const otpExpires = new Date(Date.now() + 15 * 60 * 1000); // OTP expires in 15 minutes

    const message = `Your OTP code is ${otp}. It will expire in 15 minutes.`;

    // Send SMS using Twilio
    let twilioResponse;
    try {
      twilioResponse = await twilioClient.messages.create({
        body: message,
        from: process.env.TWILIO_PHONE_NUMBER,
        to: phone,
      });
    } catch (twilioError) {
      console.error("Twilio Error:", twilioError);
      return res.status(500).json({
        message: "Failed to send OTP via SMS",
        error: twilioError.message,
      });
    }

    // Call the external API to create a conversation
    let conversationId;
    try {
      conversationId = await createConversation(email, botId);
    } catch (conversationError) {
      console.error("Conversation API Error:", conversationError);
      return res.status(500).json({
        message: "Failed to create conversation",
        error: conversationError.message,
      });
    }

    // Create a new user with the conversation ID and OTP details
    let user;
    try {
      user = await models.User.create({
        name,
        email,
        phone,
        conversationId,
        otp,
        otpExpires,
      });
    } catch (dbError) {
      console.error("Database Error:", dbError);
      return res
        .status(500)
        .json({ message: "Failed to create user", error: dbError.message });
    }

    // Return the user details
    res.status(201).json({ id: user.id, twilioResponse });
  } catch (error) {
    console.error("Internal Error:", error);
    next(AppError.internal(error.message));
  }
};

const verifyOtp = async (req, res, next) => {
  const { userId, otp } = req.body;

  try {
    // Find the user by ID
    const user = await models.User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Check if the OTP matches and is not expired
    if (user.otp !== otp) {
      return res.status(400).json({ message: "Invalid OTP" });
    }

    if (user.otpExpires < new Date()) {
      return res.status(400).json({ message: "OTP has expired" });
    }

    // If OTP is valid, clear the OTP fields
    user.otp = null;
    user.otpExpires = null;
    await user.save();

    res.status(200).json({ message: "OTP verified successfully", user });
  } catch (error) {
    console.error(error);
    next(AppError.internal());
  }
};

const updateIndustry = async (req, res, next) => {
  const { userId, industry } = req.body;

  try {
    // Find the user by ID
    const user = await models.User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Update the industry field
    user.industry = industry;
    await user.save();

    res.status(200).json({ message: "Industry updated successfully", user });
  } catch (error) {
    console.error(error);
    next(AppError.internal());
  }
};

const sendMessage = async (req, res, next) => {
  const { userId, content } = req.body;

  try {
    // Find the user by ID
    const user = await models.User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Prepare the payload for the external API
    const payload = {
      content,
      conversation_id: user.conversationId,
    };

    // Send the result to the external API
    const response = await axios.post(
      `${process.env.CODY_PORTAL}/messages`,
      payload,
      {
        headers: {
          Authorization: `Bearer ${process.env.CODY_ACCESS_TOKEN}`, // Make sure to set your access token in an environment variable
          "Content-Type": "application/json",
        },
      }
    );

    res.status(200).json({
      message: "Message sent to external API successfully",
      externalApiResponse: response.data,
    });
  } catch (error) {
    console.error(error);
    next(AppError.internal());
  }
};

const summarizeConversation = async (req, res, next) => {
  const { userId, conversation } = req.body;
  try {
    // Fetch user details from the database
    const user = await models.User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Call the external API to summarize the conversation
    const genAI = new GoogleGenerativeAI(API_KEY);
    const model = genAI.getGenerativeModel({ model: MODEL_NAME });

    // Prepare the prompt for the OpenAI API
    const prompt = `Summarize the following conversation:\n\n${conversation}`;

    const result = await model.generateContent([prompt]);

    const summarizedText =
      result.response.candidates[0].content.parts[0].text.trim();

    const srs = await summarizeSRS(conversation);

    // Create Word document with parsed SRS content
    const parsedContent = parseSRS(srs);
    const doc = new Document({
      sections: [
        {
          properties: {},
          children: parsedContent,
        },
      ],
    });

    // Generate a unique filename
    const uniqueFilename = `srs_${uuidv4()}.docx`;

    // Save the document to the filesystem
    const filePath = `./${uniqueFilename}`;
    await Packer.toBuffer(doc).then((buffer) => {
      fs.writeFileSync(filePath, buffer);
    });

    // Construct stylish email content with customer information
    const mailSubject = `Summarized Conversation of ${user.name}`;

    // Extract detailed module information and send for mockup generation
    /*const modules = await getModulesFromAI(srs);
    console.log("Modules:", modules);

    const mockupLinks = [];

    for (const module of modules) {
      const mockupFilePath = await generateMockupFromModule(module);
      mockupLinks.push({
        name: module.name,
        description: module.details,
        mockupLink: mockupFilePath,
      });
    }*/

    let mailText = `
      <html>
        <body>
          <p>Hello</p>
          <h3>summarized conversation</h3>
          <p>${summarizedText}</p>
          <hr>
          <h3>Customer Information</h3>
          <ul>
            <li><strong>Email:</strong> ${user.email}</li>
            <li><strong>Name:</strong> ${user.name}</li>
            <li><strong>Phone:</strong> ${user.phone}</li>
            <li><strong>Industry:</strong> ${
              user.industry ? user.industry : "-"
            }</li>
          </ul>
        </body>
      </html>
    `;

    // Log the email content
    console.log("Sending mockup email with content:", mailText);

    // Send the email with the attachment and mockup links
    await sendEmail(
      process.env.MAIL_RECIPIENT,
      mailSubject,
      mailText,
      filePath
    );

    fs.unlinkSync(filePath);

    res.status(200).json({
      // result,
      // srs,
      message: "email sent sucessfully",
    });
  } catch (error) {
    console.error(error);
    next(AppError.internal());
  }
};

// Helper function to generate mockup from detailed module
// const generateMockupFromModule = async (module) => {
//   try {
//     let promptDetails = "";

//     console.log("Module Details:", module.details); // Log the module details

//     if (module.details.type === "form" && module.details.fields) {
//       promptDetails = `Fields: ${module.details.fields.join(", ")}`;
//     } else if (module.details.type === "table" && module.details.fields) {
//       promptDetails = `Table Columns: ${module.details.fields.join(", ")}`;
//     } else if (module.details.type === "dashboard" && module.details.features) {
//       promptDetails = `Dashboard Features: ${module.details.features.join(
//         ", "
//       )}`;
//     } else {
//       promptDetails = JSON.stringify(module.details);
//     }

//     const prompt = `Generate a web UI mockup for the module named "${module.name}". The design should be clean and modern, similar to Figma designs. Generated image should be inside a monitor with a black frame without stand in straight view in big size. ${promptDetails}`;

//     const response = await axios.post(
//       `${process.env.OPENAI_URL}/images/generations`,
//       {
//         model: "dall-e-3",
//         quality: "hd",
//         n: 1,
//         size: "1792x1024",
//         prompt: prompt,
//       },
//       {
//         headers: {
//           Authorization: `Bearer ${process.env.OPENAI_ACCESS_TOKEN}`,
//           "Content-Type": "application/json",
//         },
//       }
//     );

//     const imageUrl = response.data.data[0].url;

//     console.log("Generated Image URL:", imageUrl);
//     return imageUrl;
//   } catch (error) {
//     console.error("Error generating mockup:", error);
//     throw error;
//   }
// };

// Helper function to parse and format SRS content
const parseSRS = (srsText) => {
  const lines = srsText.split("\n");
  const docContent = [];

  let isHeading = false; // Track if the current line is a heading

  lines.forEach((line, index) => {
    if (line.startsWith("## ")) {
      docContent.push(
        new Paragraph({
          text: line.replace("## ", ""),
          heading: HeadingLevel.HEADING_1,
          spacing: {
            after: 200,
          },
        })
      );
      isHeading = true;
    } else if (line.startsWith("**")) {
      const parts = line.split("**");
      docContent.push(
        new Paragraph({
          children: parts
            .filter(Boolean)
            .map((part, index) =>
              index % 2 === 0
                ? new TextRun(part)
                : new TextRun({ text: part, bold: true })
            ),
          spacing: {
            after: 100,
          },
        })
      );
      isHeading = false;
    } else if (line.startsWith("* ")) {
      docContent.push(
        new Paragraph({
          text: line.replace("* ", ""),
          bullet: { level: 0 },
          spacing: {
            after: 100,
          },
        })
      );
      isHeading = false;
    } else if (line.startsWith("    * ")) {
      docContent.push(
        new Paragraph({
          text: line.replace("    * ", ""),
          bullet: { level: 1 },
          spacing: {
            after: 100,
          },
        })
      );
      isHeading = false;
    } else {
      if (index > 0 && isHeading) {
        // Add spacing only if previous line was a heading
        docContent.push(
          new Paragraph({
            text: line,
            spacing: {
              after: 100,
            },
          })
        );
      } else {
        docContent.push(
          new Paragraph({
            text: line,
            spacing: {
              after: 0, // No additional spacing if not following a heading
            },
          })
        );
      }
      isHeading = false;
    }
  });

  return docContent;
};

const summarizeSRS = async (conversation) => {
  try {
    // Call the external API to summarize the conversation
    const genAI = new GoogleGenerativeAI(API_KEY);
    const model = genAI.getGenerativeModel({ model: MODEL_NAME });

    // Prepare the prompt for the OpenAI API
    const prompt = `Generate a high-level SRS based on the following conversation:\n\n${conversation}`;

    const result = await model.generateContent([prompt]);

    const srs = result.response.candidates[0].content.parts[0].text.trim();

    return srs;
  } catch (error) {
    console.error(error);
    next(AppError.internal());
  }
};

const sendEmail = async (to, subject, html, attachmentPath = null) => {
  try {
    const transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD,
      },
    });

    const mailOptions = {
      from: process.env.MAIL_FROM,
      to,
      subject,
      html,
    };

    if (attachmentPath) {
      mailOptions.attachments = [
        {
          filename: "Detailed-SRS.docx",
          path: attachmentPath,
        },
      ];
    }

    await transporter.sendMail(mailOptions);
  } catch (error) {
    throw error; // Propagate the error to handle it in the caller function
  }
};

module.exports = {
  Login,
  createUser,
  verifyOtp,
  updateIndustry,
  sendMessage,
  summarizeConversation,
  summarizeSRS,
};
