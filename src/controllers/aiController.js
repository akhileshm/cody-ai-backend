const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
const axios = require("axios");
const { createConversation } = require("../controllers/codyController");
const { summarizeSRS } = require("../controllers/userController");
const { GoogleGenerativeAI } = require("@google/generative-ai");
const { Document, Packer, Paragraph, HeadingLevel, TextRun } = require("docx");
const { v4: uuidv4 } = require("uuid");
const fs = require("fs");
const { json } = require("sequelize");

const MODEL_NAME = "gemini-1.5-flash";
const API_KEY = process.env.GEMINI_API_KEY;

const generateSRS = async (req, res, next) => {
  const { userId, conversation } = req.body;
  try {
    // Fetch user details from the database
    const user = await models.User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Call the external API to summarize the conversation
    const genAI = new GoogleGenerativeAI(API_KEY);
    const model = genAI.getGenerativeModel({ model: MODEL_NAME });

    // Prepare the prompt for the OpenAI API
    const prompt = `Summarize the following conversation:\n\n${conversation}`;

    const result = await model.generateContent([prompt]);

    const summarizedText =
      result.response.candidates[0].content.parts[0].text.trim();

    const srs = await summarizeSRS(conversation);

    return res.status(200).json({ data: srs });
  } catch (error) {
    console.error(error);
    next(AppError.internal());
  }
};

const generateImage = async (req, res, next) => {
  console.log(req.body, "Request Body");
  const { userId, srs } = req.body;
  const mockupCount = parseInt(process.env.MOCKUP_COUNT, 10);

  try {
    // Fetch user details from the database
    const user = await models.User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    if (!srs) {
      return res.status(404).json({ message: "SRS not found" });
    }

    const modules = await getModulesFromAI(srs);
    console.log("Modules:", modules);

    const mockupLinks = [];

    // Determine whether to generate all mockups or limit to the specified count
    const shouldGenerateAllMockups = isNaN(mockupCount) || mockupCount === -1;

    for (let i = 0; i < modules.length; i++) {
      const module = modules[i];

      if (shouldGenerateAllMockups || i < mockupCount) {
        const mockupFilePath = await generateMockupFromModule(module);
        mockupLinks.push({
          name: module.name,
          description: module.details,
          mockupLink: mockupFilePath,
        });
      } else {
        mockupLinks.push({
          name: module.name,
          description: module.details,
          mockupLink: "",
        });
      }
    }

    return res.status(200).json(mockupLinks);
  } catch (error) {
    console.error(error);
    next(AppError.internal());
  }
};

const generateMockupFromModule = async (module) => {
  try {
    let promptDetails = "";

    console.log("Module Details:", module.details); // Log the module details

    if (module.details.type === "form" && module.details.fields) {
      promptDetails = `Fields: ${module.details.fields.join(", ")}`;
    } else if (module.details.type === "table" && module.details.fields) {
      promptDetails = `Table Columns: ${module.details.fields.join(", ")}`;
    } else if (module.details.type === "dashboard" && module.details.features) {
      promptDetails = `Dashboard Features: ${module.details.features.join(
        ", "
      )}`;
    } else {
      promptDetails = JSON.stringify(module.details);
    }

    const prompt = `Generate a web UI mockup for the module named "${module.name}". The design should be clean and modern, similar to Figma designs. Generated image should be inside a monitor with a black frame without stand in straight view in big size. ${promptDetails}`;

    const response = await axios.post(
      `${process.env.OPENAI_URL}/images/generations`,
      {
        model: "dall-e-3",
        quality: "standard",
        //n: 1,
        //size: "1792x1024",
        prompt: prompt,
      },
      {
        headers: {
          Authorization: `Bearer ${process.env.OPENAI_ACCESS_TOKEN}`,
          "Content-Type": "application/json",
        },
      }
    );

    const imageUrl = response.data.data[0].url;

    console.log("Generated Image URL:", imageUrl);
    return imageUrl;
  } catch (error) {
    console.error("Error generating mockup:", error);
    throw error;
  }
};

// Helper function to get modules from AI
const getModulesFromAI = async (srsText) => {
  const genAI = new GoogleGenerativeAI(API_KEY);
  const model = genAI.getGenerativeModel({ model: MODEL_NAME });

  const prompt = `Analyze the following SRS and list the different modules with their details in JSON format. Each module should be an object with "name" and "details" fields. For each detail, specify the type of UI element (e.g., form, table) and list the specific fields or features:\n\n${srsText}`;

  const result = await model.generateContent([prompt]);

  // Remove surrounding ```json and ``` tags if they exist
  let modulesText = result.response.candidates[0].content.parts[0].text.trim();
  console.log("Modules Text:", modulesText);

  // Remove any backticks or potential JSON code block indicators
  modulesText = modulesText
    .replace(/```json/g, "")
    .replace(/```/g, "")
    .trim();

  // Log the cleaned text for debugging
  console.log("Cleaned Modules Text:", modulesText);

  try {
    return JSON.parse(modulesText); // Assuming the response is a JSON formatted list of modules
  } catch (error) {
    console.error("Failed to parse modules from AI response:", error);
    throw new Error("Invalid JSON format in AI response");
  }
};

module.exports = {
  generateSRS,
  generateImage,
};
