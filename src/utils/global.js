global.Sequelize = require("sequelize");
global.AppError = require("./appError");
global.models = require("../models");
global.Op = Sequelize.Op;
