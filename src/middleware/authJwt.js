const jwt = require("jsonwebtoken");

verifyToken = (req, res, next) => {
  try {
    let token = req.headers["authorization"];
    if (!token) {
      return res.status(403).send({
        message: "No token provided!",
      });
    }

    jwt.verify(token, process.env.JWT_KEY, (err, decoded) => {
      if (err) {
        next(AppError.unAuthorized());
        return;
      }
      req.userId = decoded.id;
      next();
    });
  } catch (error) {
    next(AppError.unAuthorized(error.message + "Auth error"));
  }
};

const authJwt = {
  verifyToken: verifyToken,
};

module.exports = authJwt;
