const { validationResult } = require("express-validator");
const { resetPasswordRules } = require("./profileValidation");

const validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }

  const extractedErrors = [];
  errors.array().forEach((err) => {
    extractedErrors.push({ [err.path]: err.msg });
  });

  return next(
    AppError.validation({
      errors: extractedErrors,
    })
  );
};

module.exports = {
  resetPasswordRules,
  validate,
};
