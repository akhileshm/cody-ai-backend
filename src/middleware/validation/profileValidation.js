const { check } = require("express-validator");

const resetPasswordRules = () => {
  return [
    check("oldPassword").not().isEmpty().withMessage("required"),
    check("newPassword").not().isEmpty().withMessage("required"),
    check("confirmPassword").not().isEmpty().withMessage("required"),
  ];
};

module.exports = {
  resetPasswordRules,
};
