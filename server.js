require("dotenv").config();
require("./src/utils/global");
const express = require("express");
const app = express();
const router = require("./src/routes/v1/index");
const cors = require("cors");
const corsOption = {
  credentials: true,
  //origin: process.env.ALLOWED_ORIGINS.split(","),
  origin: true,
};
app.use(cors(corsOption));
const PORT = process.env.PORT || 8000;

app.use(express.json());
app.use("/api/v1", router);
app.all("*", (req, _res, next) => {
  next(AppError.notFound(`${req.originalUrl} not found`));
});

const globalErrorHandler = require("./src/middleware/errorHandler");
app.use(globalErrorHandler);
app.listen(PORT, "0.0.0.0", () => {
  console.log(`server is running on port ${PORT}`);
});
